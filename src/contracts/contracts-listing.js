import React from 'react'
import {Link} from 'react-router-dom'
import ROUTES from '../routes'

import svc from './contracts-service'
import _ from 'lodash'

class ContractListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contracts: [],
      searchText: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.loadDebounced = _.debounce(this.load, 200)
  }

  componentWillMount() {
    this.load(this.state.searchText)
  }

  async load() {
    let res = await svc.searchContracts(this.state.searchText)

    this.setState({
      contracts: res.data
    })
  }

   handleChange(event) {
    this.setState({ searchText: event.target.value })
    this.loadDebounced(this.state.searchText)
  }


  render() {
    const contracts = this.state.contracts

    return (
      <div>
       <form onSubmit={this.handleSubmit}>
          <label>
            <input type="text" placeholder="Search contracts" value={this.state.searchText} onChange={this.handleChange} />
          </label>
        </form>

        <table className="table">
          <thead>
          <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Contact</th>
          </tr>
          </thead>
          <tbody>
            {contracts.map(s =>
              <tr key={s.id}>
                <td><Link to={ROUTES.getUrl(ROUTES.CONTRACT_DETAIL, {id: s.id})}>{s.name}</Link></td>
                <td>{('' + s.description).slice(0, 60)}</td>
                <td>{s.price}</td>
                <td>
                  <Link to={ROUTES.getUrl(ROUTES.CONTACT_DETAIL, {id: s.contact.id})}>{s.contact.name}</Link>
                </td>
              </tr>
            )}
          </tbody>
        </table>
        <div>
          <Link to={ROUTES.CONTRACT_NEW} className="btn btn-light">Create new</Link>
        </div>
      </div>

    )
  }
}

export default ContractListing
