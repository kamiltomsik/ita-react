import axios from 'axios'
import _ from 'lodash'

const svc = {
  getContracts() {
    return axios.get('http://localhost:3000/contracts?_expand=contact');
  },

  createContract(data) {
    return axios.post('http://localhost:3000/contracts', data)
  },

  getContract(id) {
      return axios.get(`http://localhost:3000/contracts/${id}?_expand=contact`);
  },

  updateContract(contract) {
    return axios.put('http://localhost:3000/contracts/' + contract.id, _.omit(contract, 'contact'))
  },

  deleteContract(id) {
    return axios.delete('http://localhost:3000/contracts/' + id);
  },

  searchContracts(searchText) {
    return axios.get('http://localhost:3000/contracts?_expand=contact&q=' + searchText) 
  },

  createContractComment(contractId, comment) {
    comment.contractId = contractId
    
    return axios.post('http://localhost:3000/comments/', comment )  
  },

  getContractComments(contractId) {
    return axios.get('http://localhost:3000/comments/?contractId=' + contractId )
  }
}

export default svc;
