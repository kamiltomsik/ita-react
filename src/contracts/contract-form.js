import React from 'react'

const ContractForm = ({ contract, contacts }) => {
  const handleChange = (e) =>
    contract[e.target.name] = e.target.value

  return (
    <form>
      <div className="form-row">
        <div className="form-group col-md-4">
          <label className="col-form-label">Name</label>
          <input name="name" type="text" className="form-control" value={contract.name} onChange={handleChange} />
        </div>
        <div className="form-group col-md-4">
          <label className="col-form-label">Price</label>
          <input name="price" type="number" className="form-control" value={contract.price} onChange={handleChange} />
        </div>
      </div>
      <select className="custom-select" name="contactId" onChange={handleChange}>
        {contacts.map(c =>
          <option key={c.id} value={c.id} selected={c.id === contract.contactId ? "selected": ""}>{c.name}</option>
        )}
      </select>
      <div className="form-group">
        <label className="col-form-label">Description</label>
        <textarea name="description" className="form-control" value={contract.description} onChange={handleChange} />
      </div>

    </form>
  )
}

export default ContractForm
