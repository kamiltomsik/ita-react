import React from 'react'
import ROUTES from '../routes'

import contactsService from './contacts-service'

import ContactForm from './contact-form'

class ContactNew extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contact: {}
    }
  }

  create() {
    contactsService.createContact(this.state.contact).then(res => {
      this.props.history.push(ROUTES.CONTACT_LISTING)
    })
  }

  render() {
    const contact = this.state.contact

    return (
      <div>
        <h2>Create New</h2>
        <ContactForm contact={contact} />
        <button className="btn btn-primary" onClick={() => this.create()}>Create</button>
      </div>
    )
  }
}

export default ContactNew
