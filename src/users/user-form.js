import React from 'react'


const UserForm = ({ user1 }) => {
  const handleChange = (e) =>
    user1[e.target.name] = e.target.value

  return (
    <form>
      <div className="form-row">
        <div className="form-group col-md-4">
          <label className="col-form-label">Login</label>
          <input name="login" type="text" className="form-control" value={user1.login} onChange={handleChange} />
        </div>
        <div className="form-group col-md-4">
          <label className="col-form-label">Name</label>
          <input name="name" type="text" className="form-control" value={user1.name} onChange={handleChange} />
        </div>
      </div>

    </form>
  )
}

export default UserForm