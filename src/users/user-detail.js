import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import usersService from './users-service'

class UserDetail extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user1: {}
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
    console.log(newProps)
  }

  async load(id) {
    const res = await usersService.getUser(id)

    this.setState({
      user1: res.data
    })
  }

  render() {
    const user1 = this.state.user1

    return (
      <div>
        <h2>Detail</h2>

        <div className="btn-group">
          <Link className="btn btn-light" to={ROUTES.getUrl(ROUTES.USER_EDIT, { id: user1.id })}>Edit</Link>
          <Link className="btn btn-danger" to={ROUTES.getUrl(ROUTES.USER_LISTING)}>Delete</Link>
        </div>

        <table>
          <tbody>
            <tr>
              <th>Login</th>
              <td>{user1.login}</td>
            </tr>
            <tr>
              <th>Name</th>
              <td>{user1.name}</td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}


export default UserDetail